/**
* Create our webservice App
*/

var express = require('express');
var fs = require('fs'); 
var http = require('http');
var request = require('request');
var app = express();


var config;
/** 
* Load config file ( remember to NEVER commit your real file )
* copy your config.example.json to config.json and put your real data
*/

try{
  var configFile = fs.readFileSync('./config.json');
  config = JSON.parse(configFile);
}catch(error){
  console.log(error);
  console.log('Please, be sure you have a correct config file');
  process.exit();
}



app.get('/', function(req, res){
   res.send('welcome to korvilo!');
});

app.post('/auth', function(req, res){

   var request = require("request");
 
   var sent = request({
       uri: "http://lamatriz.org/api/oauth/authorize",
       method: "GET",
       rejectUnauthorized: false
   });

   sent.on('error', function(error){
       /** Getting bad request beacuse not all parameter are present 
	** TODO: ADD oaut params and get token for the app
       */
      console.log(error);
      res.send(error);
   });

   sent.on('response',function(response){
       /** If OK, send token to the app
       * TODO: custom redirect depending on app 
       * TODO: redirect, first step -> url on subdomain
       */
     console.log(response);
     res.send(response);
   });
  

});

 
app.listen(3000);
